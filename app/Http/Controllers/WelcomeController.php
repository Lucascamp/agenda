<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class WelcomeController extends Controller {
	public function __construct()
	{
		$this->middleware('guest');
	}
	public function index()
	{
		return redirect()->guest('auth/login');
	}

}
