<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Ordemservico;
use App\Cliente;
use App\Contato;
use App\Telefone;
use Carbon\carbon;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Ordemservico $ordemservico, Cliente $cliente, Contato $contato, Telefone $telefone)
	{
		$this->middleware('auth');
		$this->ordemservico = $ordemservico;
		$this->cliente = $cliente;
		$this->contato = $contato;
		$this->telefone = $telefone;
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::check())
		{
			// $cargo   = Input::get('filter_cargo');
			// $filter_cargo = $this->ordemservico->orderBy('cargo')->groupBy('cargo')->lists('cargo', 'cargo');
			
   //      	if($cargo)
        	// 	$data['ordemservicos'] =  $this->ordemservico->where('cargo', $cargo)->paginate(10);
        	// else
        		//$data['ordemservicos'] = $this->ordemservico->with('cliente', 'usuario')->paginate(20);
				$data['contatos'] = $this->contato->with('telefone')->paginate(100);
			// $data['filter_cargo'] = ['' =>'Filtro por Cargo'] + $filter_cargo;
			// $data['cargo'] = $cargo;

			return redirect()->route('agenda.index');
		}

		return view('login');
	}

	public function cadastrar()
    {
        $data['usuario'] = Auth::user();
        $data['data'] = Carbon::now()->format('d/m/Y');
        $data['clientes'] = $this->cliente->lists('nome', 'id');

        return view('ordemservico.cadastrar')->with($data);
    }

    public function salvar() 
    {
        $input = array_except(Input::all(), array('_token'));

        $this->ordemservico->create($input);

        return redirect()->route('ordemservico.index');
    }

    public function editar()
    {   
        $ordemservico = Input::get('ordemservico_id');

        $data['ordemservico'] = $this->ordemservico->where('id', $ordemservico)->first();

        return Response::json($data);
    }

    public function atualizar() 
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $ordemservico = $this->ordemservico->findOrFail($input['ordemservico_id']);

        $ordemservico->update($input);

        return redirect()->route('ordemservico.index');
    }

    public function excluir($id)
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $this->ordemservico->find($id)->delete();

        return redirect()->route('ordemservico.index');
    }

}
