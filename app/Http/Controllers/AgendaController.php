<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Contato;
use App\Telefone;
use Carbon\carbon;

class AgendaController extends Controller {

	public function __construct(Contato $contato, Telefone $telefone)
	{
		$this->middleware('auth');
		$this->contato = $contato;
		$this->telefone = $telefone;
	}

	public function index()
	{
		$data['nome']   = Input::get('filter_nome');
        $data['empresa'] = Input::get('filter_empresa');
        $data['cidade'] = Input::get('filter_cidade');

        $data['filter_nome'] = ['' =>'Nome'] + $this->contato->lists('nome', 'nome');
        $data['filter_empresa'] = ['' =>'Empresa'] + $this->contato->lists('empresa', 'empresa');
        $data['filter_cidade'] = ['' =>'Cidade'] + $this->contato->lists('cidade', 'cidade');

        $contatoQuery = $this->contato->with('telefone');

        if ($data['nome'])
        {               
            $contatoQuery->where('nome', $data['nome']);
        }    

        if ($data['empresa'])
        {               
            $contatoQuery->where('empresa', $data['empresa']);
        } 

        if ($data['cidade'])
        {               
            $contatoQuery->where('cidade', $data['cidade']);
        }

        $data['contatos'] = $contatoQuery->paginate(100);

		return view('agenda.index')->with($data);
	}

	public function cadastrar()
    {
        $data['usuario'] = Auth::user();
        $data['data'] = Carbon::now()->format('d/m/Y');
        $data['clientes'] = $this->cliente->lists('nome', 'id');

        return view('ordemservico.cadastrar')->with($data);
    }

    public function addContato() 
    {
        $input = array_except(Input::all(), array('_token'));

        $contato = $this->contato->create($input);

        foreach($input['telefone'] as $key => $tel)
        {
        	$this->telefone->create([
        							'contato_id' => $contato->id,
        							'tipo' => $input['tipo'][$key],
        							'telefone' => $tel
        							]);
        }

        return redirect()->route('agenda.index');
    }

    public function editar()
    {   
        $contato = Input::get('contato_id');

        $data['contato'] = $this->contato->where('id', $contato)->first();
        $data['telefone'] = $this->telefone->where('contato_id', $contato)->get();

        return Response::json($data);
    }

    public function atualizar() 
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $contato = $this->contato->findOrFail($input['contato_id']);

        $contato->update($input);

        foreach($input['ids'] as $key => $id)
        {
        	$this->telefone->where('id', $id)
        					->update([
        							'tipo' => $input['tipo'.$id],
        							'telefone' => $input['telefone'.$id]
        							]);
        }

        foreach($input['telefone'] as $key => $tel)
        {
        	$this->telefone->create([
        							'contato_id' => $contato->id,
        							'tipo' => $input['tipo'][$key],
        							'telefone' => $tel
        							]);
        }

        return redirect()->route('agenda.index');
    }

    public function salvarTel() 
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $data['telefone'] = $this->telefone->create($input);

        return Response::json($data);
    }

    

    public function excluir($id)
    {
        $input = array_except(Input::all(), array('_token', '_method'));

        $this->ordemservico->find($id)->delete();

        return redirect()->route('ordemservico.index');
    }

}
