<?php

Route::get('/', 'WelcomeController@index');
Route::get('/logout',function(){
	Auth::logout();
	return Redirect::away('/');
});
Route::get('/login',function(){
	Auth::logout();
	return Redirect::away('/');
});

Route::get('home',  array('as' => 'ordemservico.index', 'uses' => 'HomeController@index'));

Route::get('/ordemservico/cadastrar',array('as' => 'ordemservico.cadastrar', 'uses' => 'HomeController@cadastrar'));
Route::post('/ordemservico', array('as' => 'ordemservico.salvar', 'uses' => 'HomeController@salvar'));

Route::delete('/excluir/{ordemservico}', array('as' => 'ordemservico.excluir','uses' => 'HomeController@excluir'));

Route::get('ordemservicos/filtro', 'HomeController@index');
Route::post('ordemservicos/filtro', array('as' => 'ordemservicos.filtro', 'uses' => 'HomeController@index'));

Route::post('/ordemservico/editar', array('as' => 'ordemservico.editar', 'uses' => 'HomeController@editar'));

Route::post('/ordemservico/atualizar', array('as' => 'ordemservico.atualizar', 'uses' => 'HomeController@atualizar'));

/*Agenda*/
Route::get('agenda',  array('as' => 'agenda.index', 'uses' => 'AgendaController@index'));
Route::get('agenda/filtro', 'AgendaController@index');
Route::post('agenda/filtro', array('as' => 'agenda.filtro', 'uses' => 'AgendaController@index'));

Route::post('/cadastrar/contato',array('as' => 'agenda.addContato', 'uses' => 'AgendaController@addContato'));

Route::post('/contato/editar', array('as' => 'contato.editar', 'uses' => 'AgendaController@editar'));
Route::post('/contato/atualizar', array('as' => 'contato.atualizar', 'uses' => 'AgendaController@atualizarContato'));

Route::post('/contato/salvarTel', 'AgendaController@salvarTel');

Route::post('/contato/atualizar', array('as' => 'contato.atualizar', 'uses' => 'AgendaController@atualizar'));

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
