<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateEquipamento extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'descricao'				=> 'required',
			'aplicacao'		   		=> 'required',
			'marca'		   			=> 'required',
			'modelo'		   		=> 'required',
			'serial' 	   			=> 'required',	
			'descricao_uso'	   		=> 'required',
			'data_compra'	   		=> 'required|date_format:"Y-m-d"',
			'nota_fiscal'			=> 'required',
			'valor_compra'			=> 'required',
			'fornecedor'   			=> 'required',
		];
	}
}
