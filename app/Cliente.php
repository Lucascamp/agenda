<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'cliente';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	protected $fillable = array('nome');

	public function ordemservico()
	{
		return $this->hasMany('App\Ordemservico', 'cliente_id');
	}

}
