<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'contato';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	protected $fillable = array('nome','empresa','cidade','email','endereco','observacao');

	public function telefone()
	{
		return $this->hasMany('App\Telefone', 'contato_id');
	}

}
