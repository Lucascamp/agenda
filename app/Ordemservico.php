<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\carbon;

class Ordemservico extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'ordemservico';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	protected $fillable = array('cliente_id', 'data', 'descricao', 'solucao', 'status', 'created_by');

	public function getDates(){
		return array(
			'created_at', 
			'updated_at', 
			'data'
			);
	}

	public function setDataAttribute($value)
	{
		$this->attributes['data']  = Carbon::createFromFormat('d/m/Y', $value);
	}

	public function cliente()
	{
		return $this->belongsTo('App\Cliente', 'cliente_id', 'id');
	}

	public function usuario()
	{
		return $this->belongsTo('App\User', 'created_by', 'id');
	}

}
