<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'telefone';

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	protected $softDelete = true;

	/**
	 * Define the rules for the validation.
	 *
	 * @var array
	 */
	public static $rules = array(
		
	);

	protected $fillable = array('contato_id','telefone','tipo');

	public function contato()
	{
		return $this->belongsTo('App\Contato', 'id');
	}
}
