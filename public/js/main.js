var $ = jQuery.noConflict();
//main function
jQuery(function($){ 

  $('.chosen-select').chosen({
    placeholder_text_single: "Selecione",
    no_results_text: "Nenhum resultado encontrado!",
    width:"100%",
    allow_single_deselect: true
  }).addClass('form-control');

  $('[data-tooltip="tooltip"]').tooltip();

  var token = $("#token").val();

  $('.editarBtn').click(function() {

    var ordemservico_id = $(this).attr('data_value');
    $("#ordemservico_id").val(ordemservico_id);

    $.post('/ordemservico/editar', {
      _token  : token,
      ordemservico_id: ordemservico_id,

    }, function(data) {  
     $("#descricaoedit").val(ordemservico.descricao);
     $("#solucaoedit").val(ordemservico.solucao);
     $("#statusedit").val(ordemservico.status);
   });
  }); 

  $('.editarContatoBtn').click(function() {
    $("#nome").val('');
    $("#empresa").val('');
    $("#cidade").val('');
    $("#email").val('');
    $("#endereco").val('');
    $("#observacao").val('');
    $("#inputtel").empty();
    $("#inputtipo").empty();

    var contato_id = $(this).attr('data_value');
    $("#contato_id").val(contato_id);

    $.post('/contato/editar', {
      _token  : token,
      contato_id: contato_id,

    }, function(data) {  
      var contato = data.contato;
      var telefone = data.telefone;
     $("#nome").val(contato.nome);
     $("#empresa").val(contato.empresa);
     $("#cidade").val(contato.cidade);
     $("#email").val(contato.email);
     $("#endereco").val(contato.endereco);
     $("#observacao").val(contato.observacao);

     $.each(telefone,function(index,value){
      input ="<input name='ids[]' type='hidden' style='width:30%' class='form-control' value='"+value.id+"'>";
      $("#inputids").append(input);
      input ="<input name='telefone"+value.id+"'' type='text' style='width:30%' class='form-control' value='"+value.telefone+"'>";
      $("#inputtel").append(input);
      input ="<input name='tipo"+value.id+"'' type='text' class='form-control' value='"+value.tipo+"'>";
      $("#inputtipo").append(input);
      });
   });
  }); 

  $('.addTelBtn').click(function() {
    $("#addTelContato").val($(this).attr('data_value'));
    $("#tipoAdd").val('');
    $("#telefoneAdd").val('');
  }); 

   $('.salvarTel').click(function() {
    var contato_id = $("#addTelContato").val();
    var tipo = $("#tipoAdd").val();
    var telefone = $("#telefoneAdd").val();

    $.post('/contato/salvarTel', {
      _token  : token,
      contato_id: contato_id,
      tipo: tipo,
      telefone: telefone,

    }, function(data) {  
      var telefone = data.telefone;

      $("#teltb_"+telefone.contato_id).html(telefone.tipo);
      $("#tipotb_"+telefone.contato_id).html(telefone.telefone);
      $('#modalAddTelefone').modal('hide');
   });
  }); 
    
  $(".add_field_button").click(function(e){ //on add input button click
    var x = 1;
    var max_fields      = 50; //maximum input boxes allowed
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
      x++; //text box increment
      $(".tipo_add_wrap").append('<div><input type="text" class="form-control" name="tipo[]"/></div>'); 
      $(".tel_add_wrap").append('<div><input style="width:30%" type="text" class="form-control" name="telefone[]"/></div>');
    }
  });

});//end main
