<!DOCTYPE html>
<html class="bg-white">
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        
        <link href="/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <link href="/css/bootstrap.css" rel="stylesheet">
        <link href="/css/custom.css" rel="stylesheet">
    </head>
    <body class="bg-white">

        <div class="form-box " id="login-box">
            <div class="header" style='background-color:#FF4D4D;'>Login</div>

            <form role="form" method="POST" action="/auth/login">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('usuario') }}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Senha">
                    </div>
                </div>
                <div class="footerLogin" style='background-color:#FF4D4D;'>
                    <button type="submit" class="btn bg-gray btn-block">Entrar</button>
                </div>
            </form>

        @if (count($errors) > 0)
            <br>
            <div class="alert alert-danger" style='width:330px;'>
                <strong>Ooops!</strong> Verifique os dados por favor:<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif      
        
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>
