<div class="modal fade" id="modalAddContato" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Adicionar Contato</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        {!! Form::open(array('route' => 'agenda.addContato')) !!}
        {!! Form::token() !!}
        <table class="table">
            <tr>
                <td class="tblabel" width="20%">Nome</td>
                <td colspan="2">
                    {!! Form::text('nome', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel" width="20%">Empresa</td>
                <td colspan="2">
                    {!! Form::text('empresa', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel" width="20%">Cidade</td>
                <td colspan="2">
                    {!! Form::text('cidade', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>
            </tr>
            <tr>
                <td>
                    Tipo
                </td>
                <td>
                    Telefone
                </td>
                <td>
                    <div class="pull-right"><button class="add_field_button btn btn-warning">Adicionar Campos</button></div>
                </td>
            </tr>

            <tr>
                <td>
                    {!! Form::text('tipo[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                    <div class="tipo_add_wrap"></div>
                </td>
                <td colspan="2">
                    {!! Form::text('telefone[]', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                    <div class="tel_add_wrap"></div>
                </td>
            </tr>
            <tr>
                <td class="tblabel" width="20%">E-mail</td>
                <td colspan="2">
                    {!! Form::text('email', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel" width="20%">Endereço</td>
                <td colspan="2">
                    {!! Form::text('endereco', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel" width="20%">Observação</td>
                <td colspan="2">
                    {!! Form::text('observacao', null, ['class' => 'form-control', 'style' => 'width: 100%']) !!}
                </td>
            </tr>
        </table>
        <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {!! Form::submit('Salvar', array('class' => 'btn btn-info')) !!}
        {!! Form::close() !!}
        </div>
    </div>
</div>
</div>
</div>