<div class="modal fade" id="modalAddTelefone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm">
    <div class="modal-content">
      <div class="modal-header modal-header-success">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Adicionar Telefone</font></h4>
    </div>
    <div class="modal-body" id="bodym">

        {!! Form::hidden('addTelContato', '', array('id' => 'addTelContato')); !!}
        <table class="table">
            <tr>
                <td class="tblabel" width="20%">Tipo</td>
                <td>
                    {!! Form::text('tipo', null, ['class' => 'form-control', 'style' => 'width: 100%', 'id'=>'tipoAdd']) !!}
                </td>
            </tr>
            <tr>
                <td class="tblabel" width="20%">Telefone</td>
                <td>
                    {!! Form::text('telefoneAdd', null, ['class' => 'form-control', 'style' => 'width: 100%', 'id'=>'telefoneAdd']) !!}
                </td>
            </tr>
        </table>
        <div class="modal-footer" id="footerm">

            <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
            {!! Form::submit('Salvar', array('class' => 'btn btn-success salvarTel')) !!}

            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>
</div>