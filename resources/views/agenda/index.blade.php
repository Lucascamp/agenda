@extends('layouts.master')

@section('main')

<div class="col-md-6">
    <div class="pull-left">
        <h1>Agenda</h1>
    </div>
</div>

<br>

<table class="table tabela">
    {!! Form::open(array('route' => 'agenda.filtro', 'method' => 'get')) !!}
    <tr>
        <td>
            Filtros:
        </td>
        <td>
            {!! Form::select('filter_nome', $filter_nome, $nome, array('id'=>'nomefilter', 'class'=>'chosen-select')) !!}
        </td>

        <td>
            {!! Form::select('filter_empresa', $filter_empresa , $empresa, array('id'=>'empresafilter', 'class'=>'chosen-select')) !!}
        </td>

        <td>
            {!! Form::select('filter_cidade', $filter_cidade , $cidade, array('id'=>'cidadefilter', 'class'=>'chosen-select')) !!}
        </td>

        <td>
            <button type="submit" data-tooltip="tooltip" class="btn btn-primary fa fa-filter" style="width: 41px; height:34px;" title="Filtrar tabela"></button>

            <button type="button" data-tooltip="tooltip" class="btn btn-info fa fa-eraser" style="width: 41px; height:34px;" id="limparalm" title="Limpar"></button>
        </td>
    </tr>
    {!! Form::close() !!}
</table>
<br>
<div class="col-md-9">
{!! $contatos->appends(array(
                            'filter_nome' => Input::get('filter_nome'),
                            'filter_empresa' => Input::get('filter_empresa'),
                            'filter_cidade' => Input::get('filter_cidade'),
                        ))->render() !!}
</div>                         

<div class="col-md-3">
    <div class="pull-right">
        <button type="button" data-target="#modalAddContato"  data-toggle="modal" class="btn btn-info">Adicionar Contato</button>
    </div>
</div>                        

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class='col-md-12'>    
    
    <table class="table table-hover">

        <tr style="background-color:#939393">
            <th>ID</th>
            <th>Nome</th>
            <th>Empresa</th>
            <th>Cidade</th>
            <th>Tipo</th>
            <th>Telefones</th>
            <th>E-mail</th>
            <th colspan="2">Ações</th>
        </tr>

        @foreach($contatos as $contato)
        <tr>
            <td>{{ $contato->id }}</td>
            <td>{{ $contato->nome }}</td>
            <td>{{ $contato->empresa }}</td>
            <td>{{ $contato->cidade }}</td>
            
            <td>      
            @foreach($contato->telefone as $telefone)
                {{ $telefone->tipo }} 
                @if(count($contato->telefone)>1)
                <br>
                @endif 
            @endforeach 
            <span id='teltb_{{ $contato->id }}'></span>  
            
            </td>
            <td width="10%">
            @foreach($contato->telefone as $telefone)      
                {{ $telefone->telefone }} 
                @if(count($contato->telefone)>1)
                <br>
                @endif    
            @endforeach  
            <span id='tipotb_{{ $contato->id }}'></span>
            </td>

            <td>{{ $contato->email }}</td>

            <td>
                <button type="button" data-target="#modalEditarContato" data-tooltip="tooltip" data-toggle="modal" class="btn btn-info fa fa-pencil editarContatoBtn" style="width: 41px; height:34px;" title="Editar Contato" data_value="{{ $contato->id }}"></button>
            </td>
            <td>
                <button type="button" data-target="#modalAddTelefone" data-tooltip="tooltip" data-toggle="modal" class="btn btn-success fa fa-plus addTelBtn" style="width: 41px; height:34px;" title="Adicionar Telefone" data_value="{{ $contato->id }}"></button>
            </td>
            
            </tr>
        @endforeach
    </table>
</div>

@include('agenda.partials.modal_add_contato')
@include('agenda.partials.modal_editar_contato')
@include('agenda.partials.modal_add_telefone')

@stop    
