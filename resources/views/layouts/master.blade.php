<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Omega</title>

    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/chosen.css" rel="stylesheet">

</head>

<body>
		@include('layouts.header')
        @yield('main')
</body>

    {!! HTML::script('js/jquery2.1.js') !!}
    {!! HTML::script('js/bootstrap.js') !!}
    {!! HTML::script('js/bootstrap-dialog.js') !!}
    {!! HTML::script('js/chosen.js') !!}
    {!! HTML::script('js/main.js') !!}

</html>        
