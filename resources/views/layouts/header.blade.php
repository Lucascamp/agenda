<nav class="navbar navbar-default" style='background-color:#4D4646;'>
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><font color='white'>Seja bem vindo {{ Auth::user()->nome }}</font></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        {{-- <li><a href="{{ route('ordemservico.cadastrar') }}"><font color='white'>Cadastrar nova ordem de serviço</i></a></font></li> --}}
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/logout"><i class="fa fa-sign-out"></i><font color='white'>Log out</a></font></li>
      </ul>
    </div>
  </div>
</nav>