﻿@extends('layouts.master')

@section('main')

<div class='col-md-offset-1'>

    <h1>Ordens de serviço</h1>

    <br>

</div>

<br>

<input type="hidden" id="token" value="{{ csrf_token() }}">

<div class='col-md-offset-1 col-md-10'>    
    
    <table class="table table-hover">

        <tr>
            <th>ID</th>
            <th>Cliente</th>
            <th>Data</th>
            <th>Descrição</th>
            <th>Solução</th>
            <th>Status</th>
            <th>criado por</th>
            <th colspan="2">Ações</th>
        </tr>

        @foreach($ordemservicos as $ordemservico)
        <tr>
            <td>{{ $ordemservico->id }}</td>
            <td>{{ $ordemservico->cliente->nome }}</td>
            <td>{{ $ordemservico->data->format('d/m/Y') }}</td>
            <td>{{ $ordemservico->descricao }}</td>
            <td>{{ $ordemservico->solucao }}</td>  
            <td>{{ $ordemservico->status }}</td> 
            <td>{{ $ordemservico->usuario->nome }}</td> 

            <td>
                <button type="button" data-target="#modalEditar" data-toggle="modal" class="btn btn-info editarBtn" data_value="{{ $ordemservico->id }}">Editar</button>
            </td>

            <td>
            {!! Form::open(array('method' => 'DELETE', 'route' => array('ordemservico.excluir', $ordemservico->id))) !!}
                {!! Form::button('Excluir', array('type' => 'submit', 'class' => 'btn btn-danger')) !!}
            {!! Form::close() !!}
            </td>
            
            </tr>
        @endforeach
    </table>
</div>

<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titulom"><font color='#ffffff'>Editar Ordem de serviço</font></h4>
    </div>
    <div class="modal-body" id="bodym">
        {!! Form::open(array('route' => 'ordemservico.atualizar', 'class'=>'form-inline', 'files' => true)) !!}
        {!! Form::token() !!}
        {!! Form::hidden('ordemservico_id', '', array('id' => 'ordemservico_id')); !!}<p>
        Descrição:
        {!! Form::text('descricao', null, array('class'=>'form-control', 'id'=>'descricaoedit', 'placeholder'=>'Descrição', 'style'=>'width:100%')) !!}<p>
        Solução:
        {!! Form::text('solucao', null, array('class'=>'form-control', 'id'=>'solucaoedit', 'placeholder'=>'Solução', 'style'=>'width:100%')) !!}<p>
        Status:
        {!! Form::select('status',['Aberto' => 'Aberto',
                                                'Pedente' => 'Pedente', 
                                                'Fechado' => 'Fechado'],
                                                 isset($os) ? $os->cargo : null, array('class'=>'form-control', 'style'=>'width:100%')) !!}<p>
    </div>
    <div class="modal-footer" id="footerm">

        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        {!! Form::submit('Salvar', array('class' => 'btn btn-info')) !!}

        {!! Form::close() !!}
    </div>
</div>
</div>
</div>

@stop    
