@extends('layouts.master')

@section('main')

<div class='col-md-offset-1'>

    <h1>Cadastro de Ordem de serviço</h1>

    <br>

</div>

{!! Form::open(array('route' => 'ordemservico.salvar', 'class'=>'form-inline', 'files' => true)) !!}
<input type="hidden" id="token" value="{{ csrf_token() }}">
<div class='col-md-offset-1 col-md-10'>

    <table class="table table-bordered tabela">
        <tr>
            <td>Usuário:</td>
            <td>{{ $usuario->nome }}</td>
            <td>Data</td>
            <td>{{ $data }}</td>
            {!! Form::hidden('created_by', $usuario->id) !!}
            {!! Form::hidden('data', $data) !!}
        </tr>
        <tr>
            <td>Cliente</td>
            <td>{!! Form::select('cliente_id', $clientes, isset($os) ? $os->cliente : null, array('class'=>'form-control')) !!}</td>
            <td>Status</td>
            <td>{!! Form::select('status',['Aberto' => 'Aberto',
                                                'Pedente' => 'Pedente', 
                                                'Fechado' => 'Fechado'],
                                                 isset($os) ? $os->cargo : null, array('class'=>'form-control')) !!}</td>
        </tr>

        <tr>
            <td>Descrição</td>
            <td colspan="3">{!! Form::textarea('descricao', isset($os) ? $os->email : null, array('class'=>'form-control', 'style'=>'width:100%')) !!}</td>
        </tr> 

        <tr>
            <td>Solução</td>
            <td colspan="3">{!! Form::textarea('solucao', isset($os) ? $os->setor : null, array('class'=>'form-control', 'style'=>'width:100%')) !!}</td>
        </tr>        


        </table>
        
        <div class="pull-right">
            {!! Form::submit('Cadastrar Ordem de serviço', array('class' => 'btn btn-success')) !!}
        </div>    

        {!! Form::close() !!}
        <br><br>
        @if ($errors->any())

        <br><br>
        <div class="alert-group">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </div>
        </div>
        @endif
    </table>
</div>

@stop