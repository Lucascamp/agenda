<?php

use Illuminate\Database\Seeder;
use App\Funcionario as Funcionario; // to use Eloquent Model 
use Faker\Factory as Faker;

class FuncionarioTableSeeder extends Seeder {

    public function run() {

        Funcionario::truncate(); 

        Funcionario::create( [
            'nome' => 'Pedro' ,
            'email' => 'pedro@teste.br',
            'setor' => 'TI',
            'cargo' => 'Gerente',
            'foto_url' => '',
            ] );

        Funcionario::create( [
            'nome' => 'João' ,
            'email' => 'joao@teste.br',
            'setor' => 'TI',
            'cargo' => 'Programador',
            'foto_url' => '',
            ] );

        Funcionario::create( [
            'nome' => 'Flavio' ,
            'email' => 'flavio@teste.br',
            'setor' => 'TI',
            'cargo' => 'Estagiário',
            'foto_url' => '',
            ] );

        Funcionario::create( [
            'nome' => 'Maria' ,
            'email' => 'maria@teste.br',
            'setor' => 'Administrativo',
            'cargo' => 'Secretária',
            'foto_url' => '',
            ] );
    }
}