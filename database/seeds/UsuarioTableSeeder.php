<?php

use Illuminate\Database\Seeder;
use App\User as Usuario; // to use Eloquent Model 
use Faker\Factory as Faker;
 
class UsuarioTableSeeder extends Seeder {
    public function run() {

        $faker = Faker::create('pt_BR');

        Usuario::truncate(); 

        Usuario::create( [
            'nome' => 'Administrador' ,
            'password' => Hash::make('teste2015'),
            'email' => 'admin@teste.br',
        ] );

        Usuario::create( [
            'nome' => 'teste' ,
            'password' => Hash::make('teste2015'),
            'email' => 'teste@teste.br',
        ] );

    }
}