<?php

use Illuminate\Database\Seeder;
use App\Contato as Contato; // to use Eloquent Model 
use Faker\Factory as Faker;
 
class ContatoTableSeeder extends Seeder {

    public function run() {

        $faker = Faker::create('pt_BR');

        Contato::truncate(); 

        foreach(range(1, 1000) as $index)
        {
            Contato::create([
                'nome' => $faker->firstName.' '.$faker->lastname,
                'empresa' => $faker->company,
                'cidade' => $faker->city,
                'email' => $faker->email,
            ]);
            
        }
    }
}