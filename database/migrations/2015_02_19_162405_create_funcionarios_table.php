<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('funcionarios', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->string('nome');
			$table->string('email');
			$table->string('setor');
			$table->string('cargo');
			$table->string('foto_url');

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('funcionarios');
	}

}
