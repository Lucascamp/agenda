<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelefoneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('telefone', function(Blueprint $table)
		{
			// primary key
			$table->increments('id');

			$table->integer('contato_id');
			$table->string('telefone');
			$table->string('tipo');

			$table->integer('created_by')->nullable;
			$table->integer('updated_by')->nullable;
			$table->integer('deleted_by')->nullable;

			// created_at / updated_at
			$table->timestamps();

			//deleted_at
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('telefone');
	}

}
